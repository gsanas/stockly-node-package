// const axios = require('axios')
import axios from "axios";

export async function sendMessage(roomId: string, message: string) {
  const payloadMsg = { room_id: roomId, notification: message };
  const pythonPkgUrl =
    "http://192.168.0.117:3001/python-package-controller/sendtobotserver/";
  try {
    const response = await axios.post(pythonPkgUrl, payloadMsg);
    return `[${response.status}] Message sent to Bot-Server!`;
  } catch (error) {
    return error;
  }
}
