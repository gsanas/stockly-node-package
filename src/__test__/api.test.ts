import { sendMessage } from "../index";

// test case for sendMessage
test("sendMessage method test", async () => {
  expect(
    await sendMessage(
      "!pmNnFZooaeBzPdpoBh:stockly.network",
      "Current price of INFY is: 1725.32"
    )
  ).toBe("[201] Message sent to Bot-Server!");
});
